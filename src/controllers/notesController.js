const notesService = require('./../services/notesService');

const getNotes = async (req, res) => {
  try {
    const userId = req.user.userId;
    const limit = req.params.limit;
    const offset = req.params.offset;
    console.log(req.params);
    const notes = await notesService.getNotes(userId, limit, offset);
    res.status(200).json({
      "offset": offset,
      "limit": limit,
      "count": notes.length,
      "notes": notes
    });
  } catch (error) {
    res.status(500).json({
      "message": error.message
    });
  }
}

const createNote = async (req, res) => {
  try {
    const text = req.body.text;
    const userId = req.user.userId;
    await notesService.createNote(userId, text);
    res.status(200).json({
      "message": "Success"
    });
  } catch (error) {
    res.status(500).json({
      "message": error.message
    });
  }
}

const getNote = async (req, res) => {
  try {
    const noteId = req.params.id;
    const userId = req.user.userId;
    const note = await notesService.getNote(userId, noteId);
    res.status(200).json({
      "note": {
        "_id": note._id,
        "userId": note.userId,
        "completed": note.completed,
        "text": note.text,
        "createdDate": note.createdDate
      }
    });
  } catch (error) {
    res.status(400).json({
      "message": error.message
    });
  }
}

const updateNote = async (req, res) => {
  try {
    const noteId = req.params.id;
    const userId = req.user.userId;
    const text = req.body.text;
    await notesService.updateNote(userId, noteId, text);
    res.status(200).json({
      "message": "Success"
    });
  } catch (error) {
    res.status(400).json({
      "message": error.message
    });
  }
}

const checkNote = async (req, res) => {
  try {
    const noteId = req.params.id;
    const userId = req.user.userId;
    await notesService.checkNote(userId, noteId);
    res.status(200).json({
      "message": "Success"
    });
  } catch (error) {
    res.status(400).json({
      "message": error.message
    });
  }
}

const deleteNote = async (req, res) => {
  try {
    const noteId = req.params.id;
    const userId = req.user.userId;
    await notesService.deleteNote(userId, noteId);
    res.status(200).json({
      "message": "Success"
    });
  } catch (error) {
    res.status(400).json({
      "message": error.message
    });
  }
}

module.exports = {
  getNotes,
  createNote,
  getNote,
  updateNote,
  checkNote,
  deleteNote
};
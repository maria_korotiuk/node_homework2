const { registration, loginUser } = require('./../services/authService');

const register = async (req, res) => {
  const {
    username,
    password
  } = req.body;
  try {
    await registration({ username, password });
    res.status(200).json({
      "message": "Success"
    });
  } catch (error) {
    res.status(500).json({
      "message": error
    });
  }
}

const login = async (req, res) => {
  try {
    const {
      username,
      password
    } = req.body;
    const jwt_token = await loginUser({username, password});
    res.status(200).json({
      "message": "Success",
      jwt_token
    });
  } catch (error) {
    res.status(400).json({
      "message": error.message
    });
  }
}

module.exports = {
  register,
  login
};
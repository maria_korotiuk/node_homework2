const usersService = require('./../services/usersService');

const getUser = async (req, res) => {
  try {
    const username = req.user.username;
    const user = await usersService.getUser(username);
    res.status(200).json({
      "_id": user._id,
      "username": user.username,
      "createdDate": user.createdDate
    });
  } catch (error) {
    res.status(400).json({
      "message": error.message
    });
  }
}

const deleteUser = async (req, res) => {
  try {
    await usersService.deleteUser(req.user.username);
    res.status(200).json({
      "message": "Success"
    });
  } catch (error) {
    res.status(500).json({
      "message": error.message
    });
  }
}

const updateUser = async (req, res) => {
  try {
    const {
      oldPassword,
      newPassword
    } = req.body;
    await usersService.changePassword(req.user.username, oldPassword, newPassword);
    res.status(200).json({
      "message": "Success"
    });
  } catch (error) {
    res.status(400).json({
      "message": error.message
    });
  }
}

module.exports = {
  getUser,
  deleteUser,
  updateUser
};
const bcrypt = require('bcrypt');
const jwt = require('jsonwebtoken');

const User = require('./../models/userModel');
const Credentials = require('./../models/credentialsModel');

const registration = async ({ username, password }) => {
  const credentials = new Credentials({
    username,
    password: await bcrypt.hash(password, 10)
  });
  await credentials.save();

  const user = new User({ username });
  await user.save();
}

const loginUser = async ({ username, password }) => {
  const credentials = await Credentials.findOne({ username });
  if (!credentials) {
    throw new Error('Invalid email or password');
  }
  if (!(await bcrypt.compare(password, credentials.password))) {
    throw new Error('Invalid email or password');
  }
  const token = jwt.sign({
    _id: credentials._id,
    username: credentials.username
  }, 'secret');
  return token;
}

module.exports = {
  registration,
  loginUser
}